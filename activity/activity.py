# Number 1
year = int(input("Please input a year:\n"))
if year <= 0:
	print("Invalid input/year")
elif year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

print("\n")

# Number 2
row = int(input("Enter number of rows:\n"))
col = int(input("Enter number of columns:\n"))
for x in range(row):
	for y in range(col):
		print("*", end = "")
	print("")